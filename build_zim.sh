#!/bin/bash

zim --index zim_public/notebook.zim

[ -e public/ ] && rm -rf public/

zim --export \
    --format=html --template=./export_template.html \
    --output=./public \
    --overwrite \
    --verbose \
    --index-page=index \
  zim_public/notebook.zim


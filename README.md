* export Template based on [AltDefault](https://github.com/ecodiv/ZIM-templates/tree/master/AltDefault)


* run build locally using gitlab-runner and docker
* to get the builds output, I have to mount the builds path `pwd`/builds:/builds
```bash
# run this once, next time you do not need to pull the image again
gitlab-runner exec docker pages 


# prevent pulling the same image again and mount builds output path
gitlab-runner exec docker \                        
--docker-pull-policy never \
--docker-volumes `pwd`/builds:/builds \
pages 
```